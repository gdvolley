//
// Author: Öyvind Johannessen <gathers@gmail.com>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef graphics_h
#define graphics_h

#include <iostream>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

static const int WIDTH = 640;
static const int HEIGHT = 480;
static const int SIZE = WIDTH*HEIGHT;

static SDL_Surface *screen;
static SDL_Surface *screenbuffer;
static SDL_Surface *bakgrund1;
static SDL_Surface *bakgrund2;
static SDL_Surface *bollbild;
static SDL_Surface *font;
static SDL_Surface *font2;
static SDL_Surface *bonus1;
static SDL_Surface *bonus2;
static SDL_Surface *ufo;
static SDL_Surface *ufo2;
static SDL_Surface *missile;
static SDL_Surface *missile2;
static SDL_Surface *explode[30];
static SDL_Surface *skepp[20];
static SDL_Surface *pausedbild;
static SDL_Surface *bla[10];
static SDL_Surface *gron[10];

void putpixel(SDL_Surface *surface, int x, int y, Uint32 pixel);
void copyrow(SDL_Surface *surf1, int y1,SDL_Surface *surf2, int y2);
void ritanat(SDL_Surface *surf);
Uint32 getpixel(SDL_Surface *surface, int x, int y);
Uint32 getpixelclip(SDL_Surface *surface, int x, int y);
Uint32 putpixelclip(SDL_Surface *surface, int x, int y,Uint32 c);
SDL_Surface *LoadImage(SDL_Surface *surf,std::string name,int width,int height);
void rita(SDL_Surface *source,SDL_Surface *dest,int xpos,int ypos,int w,int h);
void ritaclip(SDL_Surface *source,SDL_Surface *dest,int xpos,int ypos,int w,int h);
void ritasiffra(SDL_Surface *fontsurf,int nr,SDL_Surface *dest,int xpos,int ypos);
void ritapoang(SDL_Surface *dest, int p0, int p1, SDL_Surface *font1, SDL_Surface *font2);

#endif
