//
// C++ Implementation: Starfield
//
// Description:
//
//
// Author: Öyvind Johannessen <gathers@gmail.com>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "starfield.h"

void Starfield::positionStar(int star)
{
		bool done=false;
		int newx, newy;
		while(!done){
			newx=int(640.0*randy());
			newy=int(215.0*randy());
			done=true;
			for(int n=0;n<nr_of_stars;n++){
				if(starx[n]==newx && stary[n]==newy){
					done=false;
					break;
				}
			}
		}
		starx[star]=newx;
		stary[star]=newy;
		starc[star]=0;
		starRed[star]=0.7+randy()*0.3;
		starGreen[star]=0.7+randy()*0.3;
		starBlue[star]=1;
}


Starfield::Starfield()
{
		for(int n=0;n<nr_of_stars;n++){
			positionStar(n);
		}
}


void Starfield::updateStars()
{
	for(int n=0;n<nr_of_stars;n++){
		starc[n]=starc[n]+float(4.0*randy()-4.1*randy());
		if(int(starc[n])>=256)
			starc[n]=255;
		if(int(starc[n])<=0){
			positionStar(n);
		}
	}
}


void Starfield::drawStarsOn(SDL_Surface *surf)
{
//	Uint32 pixel;
		if(SDL_LockSurface(surf)){
			fprintf(stderr,"Couldn't lock surface to draw stars..\n");
			return;
		}
		for(int n=0;n<nr_of_stars;n++){
			putpixel(surf,starx[n],stary[n],(0xFF<<24)|(int(starc[n]*starRed[n])<<16)|(int(starc[n]*starGreen[n])<<8)|int(starc[n]*starBlue[n]));
/*			pixel=(0xFF<<24)|(int(50)<<16)|(int(140)<<8)|int(200);
			putpixel(surf,starx[n],stary[n],pixel);*/
		}
		SDL_UnlockSurface(surf);
}

