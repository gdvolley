//
// C++ Interface: bollobjekt
//
// Description:
//
//
// Author: Öyvind Johannessen <gathers@gmail.com>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef ai_h
#define ai_h

#include "bollobjekt.h"
#include "spelarobjekt.h"

class gamestate
{
public:
	bollobjekt boll;
	spelarobjekt spelare[10];
	gamestate(){
//		spelare=new spelarobjekt[antalspelare];
	}
	~gamestate(){
//		delete[] spelare;
	}
};

class listnode
{
public:
	gamestate gs;
	listnode *left,*up,*right,*last;
	float score;
	int tickstonext;
	bool visited;
	listnode(){
		left=up=right=last=NULL;
//		gs=NULL;
	}
	listnode(gamestate gs_arg){
		left=up=right=last=NULL;
		gs=gs_arg;
	}
	~listnode(){
//		delete gs;
		delete left;
		delete up;
		delete right;
	}
};

class nodelist
{
public:
	listnode *head;
	nodelist(){
		head=NULL;
	}
	~nodelist(){
		delete head;
	}
};

class aistuff
{
public:
	aistuff(){
	}
	~aistuff(){
	}
};

static listnode *currentnode;
static nodelist *nl;

int visited(listnode *node){
	return currentnode->visited==nl->head->visited;
}

int topnode(listnode *node){
	return node->left==NULL;
}

float score_classic(gamestate *gs,int spelarnr){
	float tempscore,tempscore2;
	tempscore=gs->boll.xpos-gs->spelare[spelarnr].xpos;
	tempscore*=tempscore;
//	if(tempscore<0)
//		tempscore=-tempscore;
	tempscore2=gs->boll.ypos-gs->spelare[spelarnr].ypos;
	tempscore2*=tempscore2;
//	if(tempscore2<0)
//		tempscore2=-tempscore2;
	return float(sqrt(tempscore+tempscore2));
}

float score_new(gamestate *gs,int spelarnr){
	float score;
	score = sqrt(gs->boll.distance_squared(gs->spelare[spelarnr]));
	if(gs->boll.xpos > 320 == gs->spelare[spelarnr].sida){
		if(gs->boll.ypos > gs->spelare[spelarnr].ypos && score < 40.0f)
			score=-score;
		if(gs->boll.yspd>0)
			score+=2000;
		else
			score+=gs->boll.xspd*100.0f*(gs->spelare[spelarnr].sida?1.0f:-1.0f);
	} else {
		score-=gs->boll.yspd*100.0f;
	}
	if(gs->spelare[spelarnr].ai_resetflag)
		score+=1000;
	if(gs->boll.ypos < gs->spelare[spelarnr].ypos && gs->spelare[spelarnr].xpos > 320 == gs->spelare[spelarnr].sida)
		score-=20;
//	fprintf(stderr,"%f\n",score);

	return score;
}

float score(gamestate *gs,int spelarnr){
	float score;
	if(gs->spelare[spelarnr].ai==1)
		score=score_classic(gs,spelarnr);
	else if(gs->spelare[spelarnr].ai==2)
		score=score_new(gs,spelarnr);
// 	printf("player=%d\tscore=%f\n",spelarnr,score);
	return score;
}

void runna(gamestate *gs,int tick){
	for(n=0;n<antalspelare;n++)
		gs->spelare[n].ai_resetflag=false;
	while(tick){
		tick--;
		for(n=0;n<antalspelare;n++)
			if(gs->spelare[n].connected)
				if(gs->spelare[n].ai)
					gs->spelare[n].grejsa(0);
				else
					gs->spelare[n].grejsa(0);
		for(n=0;n<antalspelare;n++){
			if(gs->spelare[n].connected){
				gs->boll.grejsamed(gs->spelare[n],0);
				gs->spelare[n].flytta(0);
			}
		}
		gs->boll.flytta(0);
		gs->boll.studsabana(0);
	}
}

void expand(listnode *node,int spelarnr){
	int oldp0=p0,oldp1=p1;
	node->left=new listnode(node->gs);
	node->left->last=node;
	node->left->gs.spelare[spelarnr].key[0]=!node->left->gs.spelare[spelarnr].key[0];
	runna(&node->left->gs,node->gs.spelare[spelarnr].aiparams_nrticks);
	node->left->score=score(&node->left->gs,spelarnr);
	node->left->visited=nl->head->visited;
	if(p0>oldp0)
		if(node->gs.spelare[spelarnr].sida==0){
			node->left->score-=500;
//			fprintf(stderr,"hej hoj sida 0\n");
		}
		else
			node->left->score+=500;
	if(p1>oldp1)
		if(node->gs.spelare[spelarnr].sida==1)
			node->left->score-=500;
		else
			node->left->score+=500;
	node->up=new listnode(node->gs);
	node->up->last=node;
	node->up->gs.spelare[spelarnr].key[1]=!node->up->gs.spelare[spelarnr].key[1];
	runna(&node->up->gs,node->gs.spelare[spelarnr].aiparams_nrticks);
	node->up->score=score(&node->up->gs,spelarnr);
	node->up->visited=nl->head->visited;
	if(p0>oldp0)
		if(node->gs.spelare[spelarnr].sida==0){
			node->up->score-=500;
//			fprintf(stderr,"hej hoj sida 0\n");
		}
		else
			node->up->score+=500;
	if(p1>oldp1)
		if(node->gs.spelare[spelarnr].sida==1)
			node->up->score-=500;
		else
			node->up->score+=500;
	node->right=new listnode(node->gs);
	node->right->last=node;
	node->right->gs.spelare[spelarnr].key[2]=!node->right->gs.spelare[spelarnr].key[2];
	runna(&node->right->gs,node->gs.spelare[spelarnr].aiparams_nrticks);
	node->right->score=score(&node->right->gs,spelarnr);
	node->right->visited=nl->head->visited;
	if(p0>oldp0)
		if(node->gs.spelare[spelarnr].sida==0){
			node->right->score-=500;
//			fprintf(stderr,"hej hoj sida 0\n");
		}
		else
			node->right->score+=500;
	if(p1>oldp1)
		if(node->gs.spelare[spelarnr].sida==1)
			node->right->score-=500;
		else
			node->right->score+=500;
	p0=oldp0;
	p1=oldp1;
}

void moveai(spelarobjekt *s){
	static int inited=0;
	int depth=0;
	float tempscore;
	if(!inited){
		inited=1;
		s->aidata=new aistuff();
		nl=new nodelist();
		nl->head=new listnode();
		nl->head->gs.boll=boll;
		for(n=0;n<antalspelare;n++)
			nl->head->gs.spelare[n]=spelare[n];
		tempscore=nl->head->gs.boll.xpos-nl->head->gs.spelare[s->spelarnr].xpos;
		if(tempscore<0)
			tempscore=-tempscore;
		nl->head->score=tempscore;
		tempscore=nl->head->gs.boll.ypos-nl->head->gs.spelare[s->spelarnr].ypos;
		if(tempscore<0)
			tempscore=-tempscore;
		nl->head->score+=tempscore;
		nl->head->visited=0;
	}
	currentnode=nl->head;
	currentnode->score=99999999.0f;
	listnode *bestnode=currentnode,*worstnode=currentnode;
	nl->head->visited=!nl->head->visited;
	while(currentnode!=NULL){
		if(!visited(currentnode)){		//done here?
			if(topnode(currentnode)){
				if(currentnode->score<bestnode->score){
					bestnode=currentnode;
				}
				if(currentnode->score>worstnode->score)
					worstnode=currentnode;
				currentnode->visited=nl->head->visited;
			}
			else{
				if(!visited(currentnode->left))
					currentnode=currentnode->left;
				else{
					if(!visited(currentnode->up))
						currentnode=currentnode->up;
					else{
						if(!visited(currentnode->right))
							currentnode=currentnode->right;
						else{
							currentnode->visited=nl->head->visited;
						}
					}
				}

			}
		}
		else
			currentnode=nl->head->last;	//move down
	}
	currentnode=bestnode;
	expand(currentnode,s->spelarnr);
	if(currentnode->left->score<bestnode->score)
		bestnode=currentnode->left;
	if(currentnode->up->score<bestnode->score)
		bestnode=currentnode->up;
	if(currentnode->right->score<bestnode->score)
		bestnode=currentnode->right;
	if(s->aiparams_goback)
		while(bestnode->last!=NULL && bestnode->last!=nl->head)
			bestnode=bestnode->last;
	spelare[s->spelarnr].key[0]=bestnode->gs.spelare[s->spelarnr].key[0];
	spelare[s->spelarnr].key[1]=bestnode->gs.spelare[s->spelarnr].key[1];
	spelare[s->spelarnr].key[2]=bestnode->gs.spelare[s->spelarnr].key[2];
//	fprintf(stderr,"fin=%f\n",bestnode->score);
//	if(rand()<s->aiparams_resetchance){
		delete nl;
		nl=new nodelist();
		inited=0;
//	}
};

void spelarobjekt::grejsai(){
	moveai(this);
//	grejsa(1);
}

#endif
