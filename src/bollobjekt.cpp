//
// C++ Implementation: bollobjekt
//
// Description:
//
//
// Author: Öyvind Johannessen <gathers@gmail.com>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "bollobjekt.h"

bollobjekt::bollobjekt()
{
        w=35;
        w2=17;
        h=35;
        h2=17;
        p0=p1=0;
        reset();
}


bollobjekt::~bollobjekt()
{
}


void bollobjekt::reset()
{
        xspd=0;
        yspd=-1;
        xpos=320+float(5.0*randy()-5.0*randy());
        ypos=100;
}


void bollobjekt::flytta(int forreal)
{
        yspd+=0.02f;
        xspd*=0.999f;
        yspd*=0.999f;
        xpos+=xspd;
        ypos+=yspd;
        if (xpos<w2) {
                xpos=(float)w2;
                xspd=-xspd;
       			if(forreal)
	                SoundHandler::Instance().PlaySound(SFX_BOUNCE_WALL, CHAN_BALL, xpos/640.0);
        }
        if (ypos<h2) {
                ypos=(float)h2;
                yspd=-yspd;
       			if(forreal)
	                SoundHandler::Instance().PlaySound(SFX_BOUNCE_WALL, CHAN_BALL, xpos/640.0);
        }
        if (xpos>639-w2) {
                xpos=639-(float)w2;
                xspd=-xspd;
       			if(forreal)
	                SoundHandler::Instance().PlaySound(SFX_BOUNCE_WALL, CHAN_BALL, xpos/640.0);
        }
        if (ypos>479-h2) {
//			ypos=479-(float)h2;
//			yspd=-yspd;
                if (xpos>320)
                        p0++;
                else
                        p1++;
                reset();
       			if(forreal)
	                SoundHandler::Instance().PlaySoundCentered(SFX_GOAL, CHAN_GLOBAL);
        }
}


int bollobjekt::krockarmed(grunka &boll)
{
        if (((xpos-boll.xpos)*(xpos-boll.xpos)+(ypos-boll.ypos)*(ypos-boll.ypos))<(w2+boll.w2)*(w2+boll.w2))
                return 1;
        return 0;
}


int bollobjekt::kansla(grunka &boll)
{
        if (((xpos-boll.xpos)*(xpos-boll.xpos)+(ypos-boll.ypos)*(ypos-boll.ypos))<5000)
                return 1;
        return 0;
}


void bollobjekt::blislagen(grunka &boll)
{
        if (!kansla(boll))
                return;
        xspd+=(xpos-boll.xpos)/10.0f;
        yspd+=(ypos-boll.ypos)/10.0f;
        boll.yspd=0;
        boll.xspd=0;
        while (kansla(boll))
                puffa(boll);
}


void bollobjekt::puffa(grunka &boll)
{
        boll.xpos-=(xpos-boll.xpos)/10.0f;
        boll.ypos-=(ypos-boll.ypos)/10.0f;
}


void bollobjekt::puffas(grunka &boll)
{
        xpos+=(xpos-boll.xpos)/10.0f;
        ypos+=(ypos-boll.ypos)/10.0f;
}


void bollobjekt::studsabana(int forreal)
{
        if (ypos>250) {
                if (xpos+w2>318 && xpos<320) {
                        xspd=-xspd;
                        xpos=float(318-w2);
	    	   			if(forreal)
			                SoundHandler::Instance().PlaySound(SFX_BOUNCE_NET, CHAN_BALL, xpos/640.0);
                }
                if (xpos-w2<322 && xpos>320) {
                        xspd=-xspd;
                        xpos=float(322+w2);
	    	   			if(forreal)
			                SoundHandler::Instance().PlaySound(SFX_BOUNCE_NET, CHAN_BALL, xpos/640.0);
                }
        }
        if (ypos+h2>250 && ypos<250 && xpos+w2>318 && xpos-w2<322) {
                studsamed(320,250);
   	   			if(forreal)
	                SoundHandler::Instance().PlaySound(SFX_BOUNCE_NET, CHAN_BALL, xpos/640.0);
        }
}


void bollobjekt::studsamed(grunka boll,int forreal) //&boll for a more solid feel of the ball
{
        float temp,temp2,v;

        temp=boll.ypos-ypos;
        temp2=boll.xpos-xpos;
        v=(float)atan(temp/temp2);
        if (temp2==0) v=1.570796327f;

        temp=xspd;
        xspd=float(xspd*cos(v)+yspd*sin(v));
        yspd=float(yspd*cos(v)-temp*sin(v));
        temp=boll.xspd;
        boll.xspd=float(boll.xspd*cos(v)+boll.yspd*sin(v));
        boll.yspd=float(boll.yspd*cos(v)-temp*sin(v));

        temp=boll.xspd;
        boll.xspd=xspd;
        xspd=temp;

        temp=xspd;
        xspd=float(xspd*cos(-v)+yspd*sin(-v));
        yspd=float(yspd*cos(-v)-temp*sin(-v));
        temp=boll.xspd;
        boll.xspd=float(boll.xspd*cos(-v)+boll.yspd*sin(-v));
        boll.yspd=float(boll.yspd*cos(-v)-temp*sin(-v));
        if(forreal)
		SoundHandler::Instance().PlaySound(SFX_BOUNCE_PLAYER, CHAN_BALL, xpos/640.0);
//		boll.xpos-=boll.xspd;
//		boll.ypos-=boll.yspd;
}


void bollobjekt::studsamed(float x,float y)
{
        float temp,temp2,v1,v2;
//		xpos-=xspd;
//		ypos-=yspd;
//		temp=ypos-ring.ypos;
//		temp2=xpos-ring.xpos;
        temp=ypos-y;
        temp2=xpos-x;
        v1=float(atan(temp/temp2));
        if (temp2==0) v1=1.570796327f;
        v1-=1.570796327f;
        temp=yspd;
        temp2=xspd;
        v2=float(atan(temp/temp2));
        if (temp2==0) v2=1.570796327f;
        v2=(v2-v1)*2;
        xspd=float(temp2*cos(v2)+temp*sin(v2));
        yspd=float(temp*cos(v2)-temp2*sin(v2));
        xpos+=(xpos-x)/10.0f;
        ypos+=(ypos-y)/10.0f;
}


void bollobjekt::grejsamed(grunka &spelare,int forreal)
{
        if (krockarmed(spelare)) {
                studsamed(spelare,forreal);
                flytta(forreal);
                studsabana(forreal);
                while (krockarmed(spelare)) {
                        puffa(spelare);
                        if (krockarmed(spelare))
                                puffas(spelare);
                }
        }
}

