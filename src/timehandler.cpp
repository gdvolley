//
// C++ Implementation: timehandler
//
// Description:
//
//
// Author: Öyvind Johannessen <gathers@gmail.com>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "timehandler.h"

TimeHandler::TimeHandler(int fps)
{
        alpha=0.03f;
        interval = Uint32(1000.0f/float(fps));
        interval_reminder = 1000.0f/float(fps) - float(interval);
        reminder_accumulator = 0;
        wake_time = SDL_GetTicks();
        average_sleeptime = interval;
        average_frametime = interval;
}


TimeHandler::~TimeHandler()
{
}


void TimeHandler::sleep()
{
        int sleep_time;
        Uint32 now;

        wake_time+=interval;
        reminder_accumulator+=interval_reminder;
        if (reminder_accumulator>1.0f) {
                reminder_accumulator-=1.0f;
                wake_time++;
        }

        now = SDL_GetTicks();
        sleep_time = wake_time - now;

        if (sleep_time>interval) {
                printf("sleep_time=%d, interval=%d!\n", sleep_time, interval);
                sleep_time=interval;
                wake_time=now;
        } else if (sleep_time<0) {
//                printf("sleep_time=%d!\n", sleep_time);
                sleep_time=0;
                wake_time=now;
        }

        if (sleep_time)
                SDL_Delay(sleep_time);

        average_sleeptime = average_sleeptime * (1.0f-alpha) + alpha * float(sleep_time);
        average_frametime = average_frametime * (1.0f-alpha) + alpha * float(now - last_frame);
        last_frame = now;
}


float TimeHandler::getFPS()
{
        return 1000.0f/average_frametime;
}

