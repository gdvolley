//
// C++ Interface: soundhandler
//
// Description:
//
//
// Author: Öyvind Johannessen <gathers@gmail.com>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef soundhandler_h
#define soundhandler_h

#include <list>
#include <map>
#include <SDL/SDL_mixer.h>

#include "randy.h"

enum Sound {
	SFX_JUMP,
	SFX_GOAL,
	SFX_PLAYER_RESET,
	SFX_BOUNCE_NET,
	SFX_BOUNCE_PLAYER,
	SFX_BOUNCE_WALL
};

enum Channel {
	CHAN_TEAM_LEFT,
	CHAN_TEAM_RIGHT,
	CHAN_BACKGROUND,
	CHAN_BALL,
	CHAN_GLOBAL
};

/**
	@author Öyvind Johannessen <gathers@gmail.com>
*/
class SoundHandler
{
public:
    inline static SoundHandler& Instance() {
        static SoundHandler instance;
        return instance;
    }

	bool InitializeAudio();

	void ShutdownAudio();

	void AddSong(const char* file);

	void PlaySongs(const bool loop, const bool random);

	void PlayNextSong();

	void PollMusic();

	void PlaySound(Sound sound, Channel channel, float panning);

	void PlaySoundCentered(Sound sound, Channel channel);

private:
	int audio_rate;
	Uint16 audio_format;
	int audio_channels;
	int audio_buffers;
	Mix_Music *music;
	std::map<Sound, Mix_Chunk*> sounds;
	std::list<const char*> playlist;
	bool loopPlaylist;
	bool randomPlaylist;

	Mix_Chunk* LoadSound(const char* file);

	void PlaySong(const char* file);

	SoundHandler();

	~SoundHandler();

	inline explicit SoundHandler(SoundHandler const&) {}

	inline SoundHandler& operator=(SoundHandler const&) { return *this; }

};

#endif
