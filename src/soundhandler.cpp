//
// C++ Implementation: soundhandler
//
// Description:
//
//
// Author: Öyvind Johannessen <gathers@gmail.com>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "soundhandler.h"

SoundHandler::SoundHandler()
{
	audio_rate = 44100;
	audio_format = MIX_DEFAULT_FORMAT; /* Signed 16-bit samples, in system byte order */
	audio_channels = 2;
	audio_buffers = 1024;
	music = NULL;
}


SoundHandler::~SoundHandler()
{
}


Mix_Chunk* SoundHandler::LoadSound(const char* file)
{
	Mix_Chunk *sample;
	sample = Mix_LoadWAV(file);
	if (!sample) {
		printf("Mix_LoadWAV(\"%s\"): %s\n", file, Mix_GetError());
	}
	return sample;
}


bool SoundHandler::InitializeAudio()
{
	if (Mix_OpenAudio(audio_rate, audio_format, audio_channels, audio_buffers)) {
		printf("Unable to open audio!\n");
		exit(1);
	}
	Mix_QuerySpec(&audio_rate, &audio_format, &audio_channels);
	sounds[SFX_JUMP] = LoadSound("sounds/boip.aif");
	sounds[SFX_PLAYER_RESET] = LoadSound("sounds/bleep.wav");
	sounds[SFX_BOUNCE_NET] = LoadSound("sounds/breathy-blip2.wav");
	sounds[SFX_BOUNCE_PLAYER] = LoadSound("sounds/breathy-blip.wav");
	sounds[SFX_BOUNCE_WALL] = LoadSound("sounds/breathy-blip2.wav");
	sounds[SFX_GOAL] = LoadSound("sounds/vidgame-bleep1.wav");
	return true;
}


void SoundHandler::ShutdownAudio()
{
	Mix_CloseAudio();
}


void SoundHandler::AddSong(const char* file)
{
	playlist.push_back(file);
}


void SoundHandler::PlaySongs(const bool loop, const bool random)
{
	loopPlaylist = loop;
	randomPlaylist = random;
	PlayNextSong();
}


void SoundHandler::PlayNextSong()
{
	if (playlist.empty())
		return;
	std::list<const char*>::iterator i = playlist.begin();
	unsigned int skipped = 0;
	while (randomPlaylist && randy()<0.5 && i!=playlist.end()) {
		i++;
		skipped++;
	}
	if (skipped>=playlist.size()-1)
		i = playlist.begin();
	PlaySong(*i);
	if (loopPlaylist)
		playlist.push_back(*i);
	playlist.erase(i);
}


void SoundHandler::PollMusic()
{
	if (!Mix_PlayingMusic())
		PlayNextSong();
}


void SoundHandler::PlaySong(const char* file)
{
	Mix_FreeMusic(music);
	music = NULL;
	music = Mix_LoadMUS(file);
	if (!music) {
		printf("Mix_LoadMUS(\"%s\"): %s\n", file, Mix_GetError());
	}
	if (music) {
		Mix_PlayMusic(music, 0);
		Mix_VolumeMusic(MIX_MAX_VOLUME/2);
	}
}


void SoundHandler::PlaySound(Sound sound, Channel channel, float panning)
{
	int right = int(255.0*panning+0.5);
	if(!Mix_SetPanning(channel, 255-right, right)) {
		printf("Mix_SetPanning: %s\n", Mix_GetError());
	}
	Mix_PlayChannel(channel, sounds[sound], 0);
}


void SoundHandler::PlaySoundCentered(Sound sound, Channel channel)
{
	Mix_PlayChannel(channel, sounds[sound], 0);
}

