//
// C++ Interface: spelarobjekt
//
// Description:
//
//
// Author: Öyvind Johannessen <gathers@gmail.com>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef spelarobjekt_h
#define spelarobjekt_h

#include <SDL/SDL.h>
#include "grunka.h"
#include "soundhandler.h"

/**
	@author Öyvind Johannessen <gathers@gmail.com>
*/
class spelarobjekt : public grunka
{
private:
        SDL_JoyAxisEvent *joystick_axisset[3];
        SDL_JoyButtonEvent *joystick_buttonset[3];
        SDL_JoyHatEvent *joystick_hatset[3];
        Channel sound_chan;

public:
        bool sida;
        bool connected;
        bool needtosend;
        bool key[3];
        int ai;
        bool aiparams_goback;
        float aiparams_resetchance;
        int aiparams_nrticks;
        void *aidata;
        bool ai_resetflag;
        char bild;
        int spelarnr;
        SDLKey keyset[3];

        bool joystick_enabled;
        
        int sh2;

        spelarobjekt();

        ~spelarobjekt();

        void reset();

        void updatekeys(bool *keys);

        void joystickSetAxis(int key, SDL_JoyAxisEvent event);
        
        void joystickSetButton(int key, SDL_JoyButtonEvent event);

        void joystickSetHat(int key, SDL_JoyHatEvent event);

        void feedJAxis(SDL_JoyAxisEvent &event);
        
        void feedJButton(SDL_JoyButtonEvent &event);

        void feedJHat(SDL_JoyHatEvent &event);

        void flytta(int forreal);

        void grejsa(int forreal);

        void grejsai();

        void grejsaiold(grunka boll);
};

#endif
