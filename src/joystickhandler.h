//
// C++ Interface: joystickhandler
//
// Description:
//
//
// Author: Öyvind Johannessen <gathers@gmail.com>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef joystickhandler_h
#define joystickhandler_h

#include <SDL/SDL.h>
#include "spelarobjekt.h"

/**
	@author Öyvind Johannessen <gathers@gmail.com>
*/
class JoystickHandler
{
private:
	SDL_Joystick **joystick;
	void ConfigurePlayer(spelarobjekt *players, spelarobjekt &player);
	bool TestIsSet(SDL_Event &event, spelarobjekt *players);

public:
	JoystickHandler();

	~JoystickHandler();

	void Configure(spelarobjekt *players, int num);
};

#endif
