//
// Author: Öyvind Johannessen <gathers@gmail.com>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "graphics.h"

//using namespace graphics_h;

// void putpixel(SDL_Surface *surface, int x, int y, Uint32 pixel)
// {
// /*	Uint32 *ptr = static_cast<Uint32 *>(surface->pixels);
// 	*(ptr + (surface->pitch * y) + x*sizeof(Uint32) )=pixel;*/
// 	Uint8 *ptr = static_cast<Uint8 *>(surface->pixels);
// 	*(ptr + (surface->pitch * y) + x*sizeof(Uint32) )=pixel;
// }

void putpixel(SDL_Surface *surface, int x, int y, Uint32 pixel)
{
    int bpp = surface->format->BytesPerPixel;
    /* Here p is the address to the pixel we want to set */
    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

    switch(bpp) {
    case 1:
        *p = pixel;
        break;

    case 2:
        *(Uint16 *)p = pixel;
        break;

    case 3:
        if(SDL_BYTEORDER == SDL_BIG_ENDIAN) {
            p[0] = (pixel >> 16) & 0xff;
            p[1] = (pixel >> 8) & 0xff;
            p[2] = pixel & 0xff;
        } else {
            p[0] = pixel & 0xff;
            p[1] = (pixel >> 8) & 0xff;
            p[2] = (pixel >> 16) & 0xff;
        }
        break;

    case 4:
        *(Uint32 *)p = pixel;
        break;
    }
}

void copyrow(SDL_Surface *surf1, int y1,SDL_Surface *surf2, int y2)
{
    int bpp = surf1->format->BytesPerPixel;
	int x=0,y=y1;
	if(y1>=480||y1<0||y2>=480||y2<0)
		return;
    /* Here p is the address to the pixel we want to set */
	if(SDL_LockSurface(surf1)){
		fprintf(stderr,"Couldn't lock surface to draw mirror effect..\n");
		return;
	}
	if(SDL_LockSurface(surf2)){
		fprintf(stderr,"Couldn't lock surface to draw mirror effect..\n");
		return;
	}
	Uint8 *p1 = (Uint8 *)surf1->pixels + y1* surf1->pitch + x * bpp;
	Uint8 *p2 = (Uint8 *)surf2->pixels + y2* surf2->pitch + x * bpp;
	switch(bpp){
	case 1:
		for(x=0;x<WIDTH*4;x++){
			p2[x]=p1[x];
			p2[x+1]=p1[x+1];
			p2[x+2]=p1[x+2];
			p2[x+3]=p1[x+3];
		}
		break;
	case 4:
		for(x=0;x<WIDTH;x++){
			((Uint32 *)p2)[x]=((Uint32 *)p1)[x];
		}
		break;
	default:
		fprintf(stderr,"Unsupported bpp! bpp of %d not handled.\n",bpp);
		exit(0);
	}
	SDL_UnlockSurface(surf2);
	SDL_UnlockSurface(surf1);
}

void ritanat(SDL_Surface *surf)
{
    int bpp = surf->format->BytesPerPixel;
	int x=0,y=0;
    /* Here p is the address to the pixel we want to set */
	if(SDL_LockSurface(surf)){
		fprintf(stderr,"Couldn't lock surface to draw net..\n");
		return;
	}
    Uint32 *p = (Uint32 *)surf->pixels;// + y* surf->pitch + x * bpp;
	p+=251*surf->pitch/4;
	p+=318;
	for(y=251;y<480;y++){
		for(x=318;x<=321;x++){
			p[0]=0xFFE22000;
//			p[0]=0xFFF8BF24;
			p++;
		}
		p+=surf->pitch/4;
		p-=4;
	}
	SDL_UnlockSurface(surf);
}

Uint32 getpixel(SDL_Surface *surface, int x, int y)
{
    int bpp = surface->format->BytesPerPixel;
    /* Here p is the address to the pixel we want to retrieve */
    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

    switch(bpp) {
    case 1:
        return *p;

    case 2:
        return *(Uint16 *)p;

    case 3:
        if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
            return p[0] << 16 | p[1] << 8 | p[2];
        else
            return p[0] | p[1] << 8 | p[2] << 16;

    case 4:
        return *(Uint32 *)p;

    default:
        return 0;       /* shouldn't happen, but avoids warnings */
    }
}

Uint32 getpixelclip(SDL_Surface *surface, int x, int y){
	if(x<0||x>=WIDTH||y<0||y>=HEIGHT)
		return 0;
	else
		return getpixel(surface,x,y);
}

Uint32 putpixelclip(SDL_Surface *surface, int x, int y,Uint32 c){
	if(x<0||x>=WIDTH||y<0||y>=HEIGHT)
		return 1;
	else
		putpixel(surface,x,y,c);
	return 0;
}

// void* LoadResource(char *name,int testlength)
// {
// 	HRSRC rec;
// 	HGLOBAL handle;
// 	void *data;
// 	int length;
// 
// 	rec=FindResource(NULL,name,RT_RCDATA);
// 	handle=LoadResource(NULL,rec);
// 	
// 	data=LockResource(handle);
// 	length=SizeofResource(NULL,rec);
// 	if(testlength!=length)
// 		return NULL;
// 	return data;
// }

SDL_Surface *LoadImage(SDL_Surface *surf,std::string name,int width,int height)
{
	SDL_Surface *loaded_image = NULL;
	std::string file = "images/";
	file.append(name);
	file.append(".png");
	loaded_image = IMG_Load(file.c_str());
	if(loaded_image == NULL){
		fprintf(stderr,"Error loading image %s\n",file.c_str());
		return NULL;
	}else{
		// Convert
		SDL_Surface *compatible_image = NULL;
		compatible_image = SDL_DisplayFormat(loaded_image);
		SDL_FreeSurface(loaded_image);
		return compatible_image;
	}
}

void rita(SDL_Surface *source,SDL_Surface *dest,int xpos,int ypos,int w,int h)
{
	Uint32 temp;
	int x,y;
	if(SDL_LockSurface(dest)){
		fprintf(stderr,"Couldn't lock surface..\n");
		return;
	}
	for(y=ypos;y<ypos+h;y++)
		for(x=xpos;x<xpos+w;x++) //?
//			if(source[(y-ypos)*w+x-xpos])
//				dest[y*WIDTH+x]=source[(y-ypos)*w+x-xpos];
			if((temp=getpixel(source,x-xpos,y-ypos))&0x00FFFFFF)
				putpixel(dest,x,y,temp);
	SDL_UnlockSurface(dest);
}

void ritaclip(SDL_Surface *source,SDL_Surface *dest,int xpos,int ypos,int w,int h)
{
	Uint32 temp;
	int x,y;
	if(SDL_LockSurface(dest)){
		fprintf(stderr,"Couldn't lock surface..\n");
		return;
	}
	for(y=ypos;y<ypos+h;y++)
		for(x=xpos;x<xpos+w;x++) //?
//			if(source[(y-ypos)*w+x-xpos])
//				dest[y*WIDTH+x]=source[(y-ypos)*w+x-xpos];
			if((temp=getpixelclip(source,x-xpos,y-ypos))&0x00FFFFFF)
				putpixelclip(dest,x,y,temp);
	SDL_UnlockSurface(dest);
}

void ritasiffra(SDL_Surface *fontsurf,int nr,SDL_Surface *dest,int xpos,int ypos)
{
	Uint32 temp;
	int x,y;
	if(SDL_LockSurface(dest)){
		fprintf(stderr,"Couldn't lock surface..\n");
		return;
	}
	for(y=ypos;y<ypos+27;y++)
		for(x=xpos;x<xpos+27;x++)
//			if(font[(y-ypos)*270+x+27*nr-xpos])
//				dest[y*WIDTH+x]=font[(y-ypos)*270+x+27*nr-xpos];
			if((temp=getpixel(fontsurf,x+27*nr-xpos,y-ypos))&0x00FFFFFF)
				putpixel(dest,x,y,temp);
	SDL_UnlockSurface(dest);
}

void ritapoang(SDL_Surface *dest, int p0, int p1, SDL_Surface *font1, SDL_Surface *font2)
{
	int t,n,siffra,antal;
//lag 0
	antal=0;
	t=1;
	while(t<=p0){
		t*=10;
		antal++;
	}
	t=1;
	for(n=antal-1;n>=0;n--){
		t*=10;
		siffra=(p0%t-p0%(t/10))/(t/10);
		ritasiffra(font1,siffra,dest,10+27*n,10);
	}
//lag 1
	antal=0;
	t=1;
	while(t<=p1){
		t*=10;
		antal++;
	}
	t=1;
	for(n=antal-1;n>=0;n--){
		t*=10;
		siffra=(p1%t-p1%(t/10))/(t/10);
		ritasiffra(font2,siffra,dest,629-27*antal+27*n,10);
	}
}
