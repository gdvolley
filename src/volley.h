//
// Author: Öyvind Johannessen <gathers@gmail.com>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef volley_h
#define volley_h

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_thread.h>
#include <math.h>

#include "bollobjekt.h"
#include "graphics.h"
#include "grunka.h"
#include "randy.h"
#include "spelarobjekt.h"

bool keys[512];

SDL_Event event;

SDL_Thread *streamthread;

#define bw2 17
#define bh2 17
#define sw2 24
#define sh2 24
#define maxspelare 128

/*FMUSIC_MODULE *mod;
FMUSIC_MODULE *tystnad;
FMUSIC_MODULE *musik[35];
FSOUND_SAMPLE *hoppljud;
FSOUND_SAMPLE *explosion1;
FSOUND_SAMPLE *explosion2;
FSOUND_SAMPLE *explosion3;
FSOUND_SAMPLE *malljud;
FSOUND_SAMPLE *missilljud;
FSOUND_SAMPLE *natljud;
FSOUND_SAMPLE *studsljud;
FSOUND_SAMPLE *slagljud;
FSOUND_STREAM *streeam;*/
char smusik[512][512];

int bakgrundsbild=1,t,n,m,tick=0,tick2=0,speed=80,tid=0,tid2=0,oldtid=0,oldtid2=0,temptime,musikbyte,lat,nrstreams,volym,p0=0,p1=0,antalspelare=2,done=0,error,connected=0,loggedin=0,shallsend=1,shallsync=0,isserver=0,forcebattle=0,paused=1;
unsigned int pausetime;
float ripplephase=0;

SDL_Surface *LoadImage(SDL_Surface *surf,std::string name,int width,int height);

	bollobjekt boll;
	spelarobjekt spelare[maxspelare];
/*
class volleyGame{
public:
	int max_players;
	int max_bollar;
	int p0,p1;
	int antalbollar;
	int antalspelare;
	bollobjekt boll[1024];
	spelarobjekt spelare[1024];
	volleyGame(){
		max_players=1024;
		p0=p1=0;
		antalbollar=0;
		antalspelare=0;
		for(int n=0;n++;n<1024)
			spelare[n].connected=0;
//		for(int n=0;n++;n<1024)
//			boll[n].connected=0;
	}
	bool addSpelare(){
		if(antalspelare>=1024)
			return 0;
		antalspelare++;
		for(int n=0;n++;n<antalspelare)
			if(!spelare[n].connected){
				spelare[n].connected=1;
				break;
			}
	}
};
*/

bool loadimages(){
	if(NULL==(bakgrund1=LoadImage(bakgrund1,"bakgrund1",640,480)))
		return 0;
	if(NULL==(bakgrund2=LoadImage(bakgrund2,"bakgrund2",640,480)))
		return 0;
	if(NULL==(bollbild=LoadImage(bollbild,"boll",35,35)))
		return 0;
	if(NULL==(font=LoadImage(font,"font",270,27)))
		return 0;
	if(NULL==(font2=LoadImage(font2,"font2",270,27)))
		return 0;
	if(NULL==(bonus1=LoadImage(bonus1,"bonus1",427,62)))
		return 0;
	if(NULL==(bonus2=LoadImage(bonus2,"bonus2",427,62)))
		return 0;
	if(NULL==(ufo=LoadImage(ufo,"ufo",41,18)))
		return 0;
	if(NULL==(ufo2=LoadImage(ufo2,"ufo2",41,18)))
		return 0;
	if(NULL==(missile=LoadImage(missile,"missile",21,7)))
		return 0;
	if(NULL==(missile2=LoadImage(missile2,"missile2",21,7)))
		return 0;
	char lala3[10]={'e','x','p','l','o','d','e',NULL,NULL,NULL};
	for(n=0;n<30;n++){
		if(n>=10){
			lala3[7]=48+(n-(n%10))/10;
			lala3[8]=48+(n%10);
		}
		else{
			lala3[7]='0';
			lala3[8]=48+n;
		}
		explode[n]=LoadImage(explode[n],lala3,40,30);
		if(explode[n]==NULL){
			printf("fel vid %d",n);
			printf(lala3,n);
			return 0;
		}
	}
/*	lala3[0]='s';
	lala3[1]='k';
	lala3[2]='e';
	lala3[3]='p';
	lala3[4]='p';
	lala3[5]=NULL;
	lala3[6]=NULL;
	lala3[7]=NULL;
	lala3[8]=NULL;
	lala3[9]=NULL;
	for(n=0;n<20;n++){
		if(n>=10){
			lala3[5]=48+(n-(n%10))/10;
			lala3[6]=48+(n%10);
		}
		else{
			lala3[5]='0';
			lala3[6]=48+n;
		}
		skepp[n]=LoadImage(skepp[n],lala3,40,40);
		if(skepp[n]==NULL){
			printf("fel vid %d",n);
			printf(lala3,n);
			return 0;
		}
	}*/
	if(NULL==(pausedbild=LoadImage(pausedbild,"paused",395,48)))
		return 0;
	char lala1[8]={'b','l','a',NULL,NULL,NULL,NULL,NULL};
	char lala2[8]={'g','r','o','n',NULL,NULL,NULL,NULL};
	for(n=1;n<10;n++){
		lala1[3]=48+n;
		if(NULL==(bla[n]=LoadImage(bla[n],lala1,49,49)))
			return 0;
		lala2[4]=48+n;
		if(NULL==(gron[n]=LoadImage(gron[n],lala2,49,49)))
			return 0;
	}
	return 1;
}

bool loadsound(){
// 	tystnad=LoadSongFromResource("tystnad");
// 	if(!tystnad)
// 		return 0;
// 
// 	char lala[8]={'m','u','s','i','k',NULL,NULL,NULL};
// 	for(n=0;n<35;n++){
// 		if(n>=10){
// 			lala[5]=48+(n-(n%10))/10;
// 			lala[6]=48+(n%10);
// 		}
// 		else{
// 			lala[5]=48+n;
// 			lala[6]=NULL;
// 		}
// 		musik[n]=LoadSongFromResource(lala);
// 		if(!musik[n])
// 			return 0;
// 	}

// 	if(!(hoppljud=FSOUND_Sample_Load(FSOUND_FREE,"hopp.wav",FSOUND_2D,0))){
// 		MessageBox(NULL,"Nu sket sig nåt","",NULL);
// 		return 0;
// 	}
// 	if(!(explosion1=FSOUND_Sample_Load(FSOUND_FREE,"explosion1.wav",FSOUND_2D,0))){
// 		MessageBox(NULL,"Nu sket sig nåt","",NULL);
// 		return 0;
// 	}
// 	if(!(explosion2=FSOUND_Sample_Load(FSOUND_FREE,"explosion2.wav",FSOUND_2D,0))){
// 		MessageBox(NULL,"Nu sket sig nåt","",NULL);
// 		return 0;
// 	}
// 	if(!(explosion3=FSOUND_Sample_Load(FSOUND_FREE,"explosion3.wav",FSOUND_2D,0))){
// 		MessageBox(NULL,"Nu sket sig nåt","",NULL);
// 		return 0;
// 	}
// 	if(!(malljud=FSOUND_Sample_Load(FSOUND_FREE,"mal.wav",FSOUND_2D,0))){
// 		MessageBox(NULL,"Nu sket sig nåt","",NULL);
// 		return 0;
// 	}
// 	if(!(missilljud=FSOUND_Sample_Load(FSOUND_FREE,"missil.wav",FSOUND_2D,0))){
// 		MessageBox(NULL,"Nu sket sig nåt","",NULL);
// 		return 0;
// 	}
// 	if(!(natljud=FSOUND_Sample_Load(FSOUND_FREE,"nat.wav",FSOUND_2D,0))){
// 		MessageBox(NULL,"Nu sket sig nåt","",NULL);
// 		return 0;
// 	}
// 	if(!(studsljud=FSOUND_Sample_Load(FSOUND_FREE,"studs.wav",FSOUND_2D,0))){
// 		MessageBox(NULL,"Nu sket sig nåt","",NULL);
// 		return 0;
// 	}
// 	if(!(slagljud=FSOUND_Sample_Load(FSOUND_FREE,"slag.wav",FSOUND_2D,0))){
// 		MessageBox(NULL,"Nu sket sig nåt","",NULL);
// 		return 0;
// 	}
// 	lat=34;
// 	mod=musik[lat];
// 	volym=128;
// 	FMUSIC_SetMasterVolume(mod,volym);
// //	FMUSIC_PlaySong(mod);
// 
// 	FMUSIC_StopSong(mod);
// 	lat=0;
// 	mod=musik[lat];
// 	volym=100;
// 	FMUSIC_SetMasterVolume(mod,volym);
	return 1;
};

void skotmusik()
{
	temptime=SDL_GetTicks();
//	temptime=FMUSIC_GetTime(mod);
	if(temptime<0){
		fprintf(stderr,"temptime<0");
		temptime=tid=oldtid=0;
	}
//	tid=int(temptime/12.5f); //80/sec
//	tid=int(temptime/5.0f); //200/sec
	tid=int(float(temptime)/(1000.0f/float(speed)));
	tid2=int(float(temptime)/(1000.0f/80.0f));
	if(speed==0)
		tick=oldtid=tid=0;
	tick=(tid-oldtid);
	tick2=(tid2-oldtid2);
	if(tick)
		oldtid=tid;
	if(tick2)
		oldtid2=tid2;
	if(tick<0){
		fprintf(stderr,"tick<0");
		tick=0;
	}
	if(tick2<0){
		fprintf(stderr,"tick2<0");
		tick2=0;
	}
//	if(musikbyte)
//		musikbyte=tid=oldtid=tick=0;
	if(keys[SDLK_p]){
		if(SDL_GetTicks()>pausetime+50){
			paused=!paused;
		}
		pausetime=SDL_GetTicks();
	}
/*	if(keys[SDLK_KP7]){
		if(FMUSIC_GetTime(mod)>100){
			FMUSIC_StopSong(mod);
			lat--;
			if(lat<0)
				lat+=35;
			mod=musik[lat];
//			tid=oldtid=0;
//			musikbyte=1;
			FMUSIC_SetMasterVolume(mod,volym);
			FMUSIC_PlaySong(mod);
		}
	}
	if(keys[SDLK_KP4]){
		FMUSIC_StopSong(mod);
		lat--;
		if(lat<0)
			lat+=35;
		mod=musik[lat];
//		tid=oldtid=0;
//		musikbyte=1;
		FMUSIC_SetMasterVolume(mod,volym);
		FMUSIC_PlaySong(mod);
	}
	if(keys[SDLK_KP0]){
		FMUSIC_StopSong(mod);
		mod=tystnad;
//		tid=oldtid=0;
//		musikbyte=1;
		FMUSIC_SetMasterVolume(mod,volym);
		FMUSIC_PlaySong(mod);
	}
	if(keys[SDLK_KP8]){
		volym++;
		if(volym>255)
			volym=255;
		FMUSIC_SetMasterVolume(mod,volym);
	}
	if(keys[SDLK_KP5]){
		volym--;
		if(volym<0)
			volym=0;
		FMUSIC_SetMasterVolume(mod,volym);
	}
	if(keys[SDLK_KP6]){
		FMUSIC_StopSong(mod);
		lat++;
		if(lat>=35)
			lat-=35;
		mod=musik[lat];
//		tid=oldtid=0;
//		musikbyte=1;
		FMUSIC_SetMasterVolume(mod,volym);
		FMUSIC_PlaySong(mod);
	}
	if(keys[SDLK_KP9]){
		if(FMUSIC_GetTime(mod)>100){
			FMUSIC_StopSong(mod);
			lat++;
			if(lat>=35)
				lat-=35;
			mod=musik[lat];
//			tid=oldtid=0;
//			musikbyte=1;
			FMUSIC_SetMasterVolume(mod,volym);
			FMUSIC_PlaySong(mod);
		}
	}*/
	if(keys[SDLK_KP_PLUS]){
		speed++;
		oldtid=tid=int(float(temptime)/(1000.0f/float(speed)));
	}
	if(keys[SDLK_KP_MINUS]){
		speed--;
		if(speed<0)
			speed=0;
		oldtid=tid=int(float(temptime)/(1000.0f/float(speed)));
	}

/*	if(antalspelare<maxspelare && keys[VK_ADD]){
		if(!(tid<gammeltid+50 && tid>=gammeltid)){
			antalspelare++;
			gammeltid=tid;
		}
	}
	if(antalspelare>0 && keys[VK_SUBTRACT]){
		if(!(tid<gammeltid+50 && tid>=gammeltid)){
			antalspelare--;
			gammeltid=tid;
		}
	}*/
	if(keys[27])
		done=1;
	if(paused)
		tick=0;
	SoundHandler::Instance().PollMusic();
}

void handlemessages(){
	while(SDL_PollEvent(&event)){
		switch(event.type){
			case SDL_KEYDOWN:
				(void)rand();
				keys[event.key.keysym.sym]=1;
				if((event.key.keysym.sym==SDLK_RETURN)&&(event.key.keysym.mod&KMOD_ALT)){
					paused=1;
					if(screenbuffer->flags&SDL_FULLSCREEN)
						screenbuffer=SDL_SetVideoMode(WIDTH,HEIGHT,32,SDL_HWSURFACE|SDL_DOUBLEBUF|SDL_HWACCEL);
					else
						screenbuffer=SDL_SetVideoMode(WIDTH,HEIGHT,32,SDL_FULLSCREEN|SDL_HWSURFACE|SDL_DOUBLEBUF|SDL_HWACCEL);
					if(screenbuffer==NULL){
						fprintf(stderr,"Couldn't set 640x480x32 video mode: %s\n",SDL_GetError());
						done=1;
					}
				}
				if(event.key.keysym.sym==SDLK_b)
					forcebattle=1;
				if(event.key.keysym.sym==SDLK_1)
					bakgrundsbild=1;
				if(event.key.keysym.sym==SDLK_2)
					bakgrundsbild=2;
				if(event.key.keysym.sym==SDLK_F1)
					spelare[0].bild--;
				if(event.key.keysym.sym==SDLK_F2)
					spelare[0].bild++;
				if(event.key.keysym.sym==SDLK_F11)
					spelare[1].bild--;
				if(event.key.keysym.sym==SDLK_F12)
					spelare[1].bild++;
				if(spelare[0].bild>=10)
					spelare[0].bild=1;
				if(spelare[0].bild<=0)
					spelare[0].bild=9;
				if(spelare[1].bild>=10)
					spelare[1].bild=1;
				if(spelare[1].bild<=0)
					spelare[1].bild=9;
			break;
			case SDL_KEYUP:
				keys[event.key.keysym.sym]=0;
			break;
			case SDL_JOYAXISMOTION:
				for(int n=0;n<antalspelare;n++)
					spelare[n].feedJAxis(event.jaxis);
			break;
			case SDL_JOYBUTTONDOWN:
			case SDL_JOYBUTTONUP:
				for(int n=0;n<antalspelare;n++)
					spelare[n].feedJButton(event.jbutton);
			break;
			case SDL_JOYHATMOTION:
				for(int n=0;n<antalspelare;n++)
					spelare[n].feedJHat(event.jhat);
			break;
			case SDL_QUIT:
				done=1;
			break;
			default:
			break;
		}
	}
}

#include "starbattle.h"
#include "ai.h"

#endif
