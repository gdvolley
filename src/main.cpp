//
// Author: Öyvind Johannessen <gathers@gmail.com>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <SDL/SDL.h>
#include "joystickhandler.h"
#include "volley.h"
#include "soundhandler.h"
#include "starfield.h"
#include "timehandler.h"

using namespace std;

int main(int argc,char* argv[])
{
	int n;
	SDL_Rect rita_rect;
//	SetPriorityClass(GetCurrentProcess(), HIGH_PRIORITY_CLASS); //good for high-resolution win32 timers?
	seed_randy();
	boll=bollobjekt();
	boll.reset();
	connected=1;
	spelare[0].keyset[0]=SDLK_a;
	spelare[0].keyset[1]=SDLK_w;
	spelare[0].keyset[2]=SDLK_d;
	spelare[0].bild=9;
	spelare[0].sida=0;
	spelare[0].reset();
	spelare[0].connected=1;
	spelare[0].spelarnr=0;
	spelare[0].ai=0;
	spelare[0].aiparams_goback=0;
	spelare[0].aiparams_resetchance=1.9f;
	spelare[0].aiparams_nrticks=20;
	spelare[1].keyset[0]=SDLK_LEFT;
	spelare[1].keyset[1]=SDLK_UP;
	spelare[1].keyset[2]=SDLK_RIGHT;
	spelare[1].bild=2;
	spelare[1].sida=1;
	spelare[1].reset();
	spelare[1].connected=1;
	spelare[1].spelarnr=1;
	spelare[1].ai=2;
	spelare[1].aiparams_goback=0;
	spelare[1].aiparams_resetchance=1.9f;
	spelare[1].aiparams_nrticks=20;
	if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO)<0){
		fprintf(stderr,"Couldn't initialize SDL: %s\n",SDL_GetError());
		return 1;
	}
	atexit(SDL_Quit);
	
	SoundHandler::Instance().InitializeAudio();
	
	screen=SDL_SetVideoMode(WIDTH,HEIGHT,32,SDL_HWSURFACE|SDL_DOUBLEBUF|SDL_HWACCEL);
	if(screen==NULL){
		fprintf(stderr,"Couldn't set 640x480x32 video mode: %s\n",SDL_GetError());
		return 1;
	}

	if(!loadimages()){
		fprintf(stderr,"Couldn't load images.\n");
		return 1;
	}

	screenbuffer=SDL_CreateRGBSurface(SDL_HWSURFACE|SDL_HWACCEL,WIDTH,HEIGHT,32,0x00FF0000,0x0000FF00,0x000000FF,0xFF000000);
	screenbuffer=screen;

// 	if(!FSOUND_Init(44100,32,0))
// 		return 0;
// 
// 	if(!loadsound())
// 		return 1;
// 	FMUSIC_PlaySong(mod);

	JoystickHandler *joystick = new JoystickHandler();
	if(SDL_NumJoysticks()>0)
 		joystick->Configure(spelare, 0);
	if(SDL_NumJoysticks()>1)
  		joystick->Configure(spelare, 1);

	Starfield *stars = new Starfield();
	TimeHandler timeHandler(125);

	SoundHandler::Instance().AddSong("music/01-berit_och_hennes_apa.mod");
	SoundHandler::Instance().PlaySongs(true, true);

	int locallat=lat;
	while(!done)
    {
//		if(locallat!=lat){
//			FSOUND_Stream_Close(streeam);
//			locallat=lat;
//			fprintf(stdout,"%d\n",locallat);
//			streeam=FSOUND_Stream_OpenFile(smusik[locallat],FSOUND_2D,0);
//			FSOUND_Stream_Play(FSOUND_FREE,streeam);
//		}

		timeHandler.sleep();
		skotmusik();
		handlemessages();
		
		for(n=0;n<antalspelare;n++)
			if(spelare[n].connected)
				if(!spelare[n].ai)
					spelare[n].updatekeys(keys);
		
		while(tick){
			tick--;
			for(n=0;n<antalspelare;n++){
				if(spelare[n].connected){
					if(spelare[n].ai)
						spelare[n].grejsai();
					spelare[n].grejsa(1);
					boll.grejsamed(spelare[n],1);
					spelare[n].flytta(1);
				}
			}
			boll.flytta(1);
			p0+=boll.p0;
			boll.p0=0;
			p1+=boll.p1;
			boll.p1=0;
			boll.studsabana(1);
		}
		while(tick2){
			tick2--;
			ripplephase+=0.03f;
			if(ripplephase>=1000.0f)
				ripplephase-=1000.0f;
			if(bakgrundsbild==1){
				stars->updateStars();
				movebattle();
			}
		}
		if(bakgrundsbild==1){
			SDL_BlitSurface(bakgrund1,NULL,screenbuffer,NULL);
			stars->drawStarsOn(screenbuffer);
			drawbattle(screenbuffer);
		}
		if(bakgrundsbild==2){
			SDL_BlitSurface(bakgrund2,NULL,screenbuffer,NULL);
			for(n=231;n<480;n++)
				copyrow(bakgrund2,int(231-(n-231)/1.1f+((n-231)/(480.0f-231.0f))*10.0f*sin(ripplephase+n/10.0f)*sin(ripplephase*2.3f+n/24.0f)),screenbuffer,n);
		}
		ritanat(screenbuffer);
		if(connected)
			ritapoang(screenbuffer, p0, p1, font, font2);
		for(n=0;n<antalspelare;n++){
			if(spelare[n].connected){
				if(spelare[n].sida)
					rita(bla[spelare[n].bild],screenbuffer,int(spelare[n].xpos)-sw2,int(spelare[n].ypos)-sh2,49,49);
				else
					rita(gron[spelare[n].bild],screenbuffer,int(spelare[n].xpos)-sw2,int(spelare[n].ypos)-sh2,49,49);
			}
		}
		rita(bollbild,screenbuffer,int(boll.xpos)-bw2,int(boll.ypos)-bh2,35,35);
/*		rita_rect.x=int(boll.xpos)-bw2;
		rita_rect.y=int(boll.ypos)-bh2;
		SDL_BlitSurface(bollbild,NULL,screenbuffer,&rita_rect);*/
		if(!speed || paused){
			rita(pausedbild,screenbuffer,640/2-395/2,480/2-48/2,395,48);
/*			rita_rect.x=640/2-395/2;
			rita_rect.y=480/2-48/2;
			SDL_BlitSurface(pausedbild,NULL,screenbuffer,&rita_rect);*/
		}
		SDL_Flip(screenbuffer);
    }

	delete stars;
	delete joystick;
	SoundHandler::Instance().ShutdownAudio();

	return 0;
}
