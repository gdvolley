//
// C++ Interface: bollobjekt
//
// Description:
//
//
// Author: Öyvind Johannessen <gathers@gmail.com>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef bollobjekt_h
#define bollobjekt_h

#include <math.h>
#include "grunka.h"
#include "randy.h"
#include "soundhandler.h"

/**
	@author Öyvind Johannessen <gathers@gmail.com>
*/
class bollobjekt : public grunka
{
public:
        int p0, p1;
        
        bollobjekt();

        ~bollobjekt();

        void reset();

        void flytta(int forreal);

        int krockarmed(grunka &boll);

        int kansla(grunka &boll);

        void blislagen(grunka &boll);

        void puffa(grunka &boll);

        void puffas(grunka &boll);

        void studsabana(int forreal);

        void studsamed(grunka boll,int forreal);

        void studsamed(float x,float y);

        void grejsamed(grunka &spelare,int forreal);
};

#endif
