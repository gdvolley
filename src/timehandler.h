//
// C++ Interface: timehandler
//
// Description:
//
//
// Author: Öyvind Johannessen <gathers@gmail.com>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef timehandler_h
#define timehandler_h

#include <SDL/SDL.h>

/**
	@author Öyvind Johannessen <gathers@gmail.com>
*/
class TimeHandler
{
private:
        Uint32 wake_time, last_frame;
        int interval;
        float average_frametime, average_sleeptime;
        float alpha;
        float interval_reminder, reminder_accumulator;

public:
        TimeHandler(int fps);

        ~TimeHandler();

        void sleep();

        float getFPS();

};

#endif
