//
// C++ Implementation: spelarobjekt
//
// Description:
//
//
// Author: Öyvind Johannessen <gathers@gmail.com>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "spelarobjekt.h"

spelarobjekt::spelarobjekt()
{
        sh2=24;
        joystick_enabled=false;
}


spelarobjekt::~spelarobjekt()
{
        key[0]=0;
        key[1]=0;
        key[2]=0;
        ai_resetflag=false;
}


void spelarobjekt::reset()
{
        if (sida) {
                xpos=589;
                ypos=50;
                sound_chan=CHAN_TEAM_RIGHT;
        } else {
                xpos=50;
                ypos=50;
                sound_chan=CHAN_TEAM_LEFT;
        }
        w=49;
        w2=24;
        h=49;
        h2=24;
        xspd=0;
        yspd=0;
        needtosend=1;
        ai_resetflag=true;
}


void spelarobjekt::updatekeys(bool *keys)
{
        if (joystick_enabled)
            return;
        if (key[0]!=keys[keyset[0]]) {
                key[0]=keys[keyset[0]];
                needtosend=1;
        }
        if (key[1]!=keys[keyset[1]]) {
                key[1]=keys[keyset[1]];
                needtosend=1;
        }
        if (key[2]!=keys[keyset[2]]) {
                key[2]=keys[keyset[2]];
                needtosend=1;
        }
}


void spelarobjekt::joystickSetAxis(int key, SDL_JoyAxisEvent event)
{
		SDL_JoyAxisEvent *newevent = new SDL_JoyAxisEvent;
		newevent->which=event.which;
		newevent->axis=event.axis;
		newevent->value=event.value;
		joystick_axisset[key]=newevent;
		joystick_buttonset[key]=NULL;
		joystick_hatset[key]=NULL;
}


void spelarobjekt::joystickSetButton(int key, SDL_JoyButtonEvent event)
{
		SDL_JoyButtonEvent *newevent = new SDL_JoyButtonEvent;
		newevent->which=event.which;
		newevent->button=event.button;
		joystick_axisset[key]=NULL;
		joystick_buttonset[key]=newevent;
		joystick_hatset[key]=NULL;
}


void spelarobjekt::joystickSetHat(int key, SDL_JoyHatEvent event)
{
		SDL_JoyHatEvent *newevent = new SDL_JoyHatEvent;
		newevent->which=event.which;
		newevent->hat=event.hat;
		newevent->value=event.value;
		joystick_axisset[key]=NULL;
		joystick_buttonset[key]=NULL;
		joystick_hatset[key]=newevent;
}


void spelarobjekt::feedJAxis(SDL_JoyAxisEvent &event)
{
		if (!joystick_enabled)
			return;
		for (int n=0; n<3; n++) {
				if (joystick_axisset[n] != NULL) {
						if ( joystick_axisset[n]->which == event.which && joystick_axisset[n]->axis == event.axis ) {
								bool pressed = false;
								if(event.value>32767/2)
									if (joystick_axisset[n]->value > 0)
										pressed = true;
								if(event.value<-32767/2)
									if (joystick_axisset[n]->value < 0)
										pressed = true;
								if (key[n]!=pressed) {
										key[n]=pressed;
										needtosend=1;
								}
						}
				}
		}
}


void spelarobjekt::feedJButton(SDL_JoyButtonEvent &event)
{
		if (!joystick_enabled)
			return;
		for (int n=0; n<3; n++) {
		        if (joystick_buttonset[n] != NULL) {
/*		        		printf("STORE which=%d\tbutton=%d\tstate=%d\n",joystick_buttonset[n]->which,joystick_buttonset[n]->button,key[n]);
		        		printf("EVENT which=%d\tbutton=%d\tstate=%d\n",event.which,event.button,event.state);*/
                		if ( joystick_buttonset[n]->which == event.which && joystick_buttonset[n]->button == event.button ) {
                				bool pressed = event.state == SDL_PRESSED;
						        if (key[n]!=pressed) {
                						key[n]=pressed;
        		        				needtosend=1;
        		        		}
                		}
		        }
		}
}


void spelarobjekt::feedJHat(SDL_JoyHatEvent &event)
{
		if (!joystick_enabled)
			return;
		for (int n=0; n<3; n++) {
		        if (joystick_hatset[n] != NULL) {
                		if ( joystick_hatset[n]->which == event.which && joystick_hatset[n]->hat == event.hat ) {
                				bool pressed = joystick_hatset[n]->value & event.value;
						        if (key[n]!=pressed) {
                						key[n]=pressed;
        		        				needtosend=1;
        		        		}
                		}
		        }
		}
}


void spelarobjekt::flytta(int forreal)
{
        xpos+=xspd;
        ypos+=yspd;
        if (xpos<w2) {
                xpos=(float)w2;
                xspd=0;
        }
        if (ypos<h2) {
                ypos=(float)h2;
                yspd=0;
        }
        if (xpos>639-w2) {
                xpos=639-(float)w2;
                xspd=0;
        }
        if (ypos>479-h2) {
                ypos=479-(float)h2;
                yspd=0;
                if (sida && xpos<320) {
			if(forreal)
				SoundHandler::Instance().PlaySound(SFX_PLAYER_RESET, sound_chan, xpos/640.0);
			reset();
                }
                if (!sida && xpos>320) {
			if(forreal)
				SoundHandler::Instance().PlaySound(SFX_PLAYER_RESET, sound_chan, xpos/640.0);
			reset();
                }
        }
        if (xpos-w2<322 && xpos+w2>318 && ypos>250){
		if(forreal)
			SoundHandler::Instance().PlaySound(SFX_PLAYER_RESET, sound_chan, xpos/640.0);
		reset();
        }
        if (xpos-w2<322 && xpos+w2>318 && ypos+w2>250)
                if ((xpos-320)*(xpos-320)+(ypos-250)*(ypos-250)<w2*w2){
			if(forreal)
				SoundHandler::Instance().PlaySound(SFX_PLAYER_RESET, sound_chan, xpos/640.0);
			reset();
                }
}


void spelarobjekt::grejsa(int forreal)
{
        if (key[1]) {
                if(ypos==479-sh2)
                	if(forreal)
				SoundHandler::Instance().PlaySound(SFX_JUMP, sound_chan, xpos/640.0);
                if (yspd<=0 && ypos>300+sh2)
                        yspd-=(ypos-300+sh2)*0.0035f;
        }
        if (key[0])
                xspd-=0.25f;
        if (key[2])
                xspd+=0.25f;
        if (ypos<479-sh2)
                yspd+=0.17f;
        xspd*=0.94f;
}


//	void spelarobjekt::grejsai();


void spelarobjekt::grejsaiold(grunka boll)
{
        if (sida == boll.xpos>320) {
                if (ypos>boll.ypos || !(rand()%30)) {
                        /*				if(ypos==479-sh2)
                        					FSOUND_PlaySound(FSOUND_FREE,hoppljud);*/
                        if (yspd<=0 && ypos>300+sh2)
                                yspd-=(ypos-300+sh2)*0.0035f;
                }
                if (xpos>boll.xpos || !(rand()%30))
                        xspd-=0.25f;
                if (xpos<boll.xpos || !(rand()%30))
                        xspd+=0.25f;
        }
        if (ypos<479-sh2)
                yspd+=0.17f;
        xspd*=0.94f;
}

