//
// C++ Interface: Starfield
//
// Description:
//
//
// Author: Öyvind Johannessen <gathers@gmail.com>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef starfield_h
#define starfield_h

#include "graphics.h"
#include "randy.h"

/**
	@author Öyvind Johannessen <gathers@gmail.com>
*/
class Starfield
{
private:
	static const int nr_of_stars = 2000;
	int starx[nr_of_stars], stary[nr_of_stars];
	float starc[nr_of_stars];
	float starRed[nr_of_stars];
	float starGreen[nr_of_stars];
	float starBlue[nr_of_stars];
	void positionStar(int star);

public:
	Starfield();
	void updateStars();
	void drawStarsOn(SDL_Surface *surf);
};

#endif
