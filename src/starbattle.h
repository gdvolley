//
// Author: Öyvind Johannessen <gathers@gmail.com>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "randy.h"

const int UFONR=30;
static float ufox[UFONR],ufoy[UFONR],ufoz[UFONR],missilex[100],missiley[100],missilez[100];
static float ufox2[UFONR],ufoy2[UFONR],ufoz2[UFONR],missilex2[100],missiley2[100],missilez2[100];
static float medely,medely2,targety,targety2;
static int ufonr,ufonr2,kills,kills2;
static int inited=0;
static int battle=0;
static unsigned int bonus1delay=0,bonus2delay=0;

void movebattle()
{
	if(!battle)
		if(!randy(100000)||forcebattle){
			forcebattle=0;
			battle=1;
			inited=0;
/*			streeam=FSOUND_Stream_OpenFile("battle.mp3",FSOUND_2D,0);
			if(streeam==NULL)
				fprintf(stderr,"Fel på battle.mp3");
			else{
				FMUSIC_SetMasterVolume(mod,0);
				FSOUND_Stream_Play(FSOUND_FREE,streeam);
			}*/
//			printf("battle begin");
		}
	if(!inited){
		inited=1;
		kills=kills2=0;
		targety=targety2=100.0f;
		for(n=0;n<UFONR;n++){
			ufox[n]=float(-400-41+randy()*320.0);
			ufox2[n]=float(640+400-randy()*320.0);
			ufoy[n]=float(randy()*215.0);
			ufoy2[n]=float(randy()*215.0);
			ufoz[n]=1;
			ufoz2[n]=1;
		}
		for(n=0;n<100;n++){
			missilez[n]=0;
			missilez2[n]=0;
		}
	}
	if(battle){
		battle=0;
		ufonr=ufonr2=0;
		medely=medely2=0.0f;
		for(n=0;n<UFONR;n++){
			if(int(ufoz[n])==1){
				battle=1;
				ufonr++;
				medely+=ufoy[n];
				ufox[n]+=float(-0.12+randy()*0.7);
				ufoy[n]=float(ufoy[n]+randy()*0.01-randy()*0.01+0.1*sin(ufox[n]/6.0+ripplephase/7.0)+randy()*0.0012*sin(2+ufox[n]/9.0+ripplephase/10.0));
				ufoy[n]-=(ufoy[n]-targety)/4000.0f;
				if(int(ufox[n])>=640)
					ufoz[n]=0;
	//				ufox[n]=-100;
				if(int(ufoy[n])>=215)
					ufoy[n]=215;
				if(int(ufoy[n])<0)
					ufoy[n]=0;
				if(ufox[n]>=0&&ufox[n]<600&&randy()>0.998)
					if(int(missilez[m=randy(100)])==0){
						missilez[m]=1;
						missilex[m]=ufox[n]+31;
						missiley[m]=ufoy[n]+9;
// 						FSOUND_PlaySound(FSOUND_FREE,missilljud);
					}
//				ritaclip(ufo,surf,int(ufox[n]),int(ufoy[n]),41,18);
			}
			if(int(ufoz2[n])==1){
				battle=1;
				ufonr2++;
				medely2+=ufoy2[n];
				ufox2[n]-=float(-0.12+randy()*0.7);
				ufoy2[n]=float(ufoy2[n]+randy()*0.01-randy()*0.01+0.1*sin(ufox2[n]/6.0+ripplephase/7.0)+randy()*0.0012*sin(2+ufox2[n]/9.0+ripplephase/10.0));
				ufoy2[n]-=(ufoy2[n]-targety2)/4000.0f;
				if(int(ufox2[n])<-41)
					ufoz2[n]=0;
	//				ufox2[n]=640+100;
				if(int(ufoy2[n])>=215)
					ufoy2[n]=215;
				if(int(ufoy2[n])<0)
					ufoy2[n]=0;
				if(ufox2[n]<640-41&&ufox2[n]>0&&randy()>0.998)
					if(int(missilez2[m=randy(100)])==0){
						missilez2[m]=1;
						missilex2[m]=ufox2[n]-7;
						missiley2[m]=ufoy2[n]+9;
// 						FSOUND_PlaySound(FSOUND_FREE,missilljud);
					}
//				ritaclip(ufo2,surf,int(ufox2[n]),int(ufoy2[n]),41,18);
			}
			if(ufoz[n]>1){
				battle=1;
				ufoz[n]+=0.4f;
				if(int(ufoz[n])-2>=30)
					ufoz[n]=0;
			}
			if(ufoz2[n]>1){
				battle=1;
				ufoz2[n]+=0.4f;
				if(int(ufoz2[n])-2>=30)
					ufoz2[n]=0;
			}
		}
		if(int(ufonr2))
			targety=medely2/float(ufonr2);
		if(int(ufonr))
			targety2=medely/float(ufonr);
		for(n=0;n<100;n++){
			if(int(missilez[n])==1){
				battle=1;
				for(m=0;m<UFONR;m++)
					if((int(ufoz2[m])==1)&&missilex[n]+21>ufox2[m]&&missilex[n]<ufox2[m]+41&&missiley[n]+7>ufoy2[m]&&missiley[n]<ufoy2[m]+18){
	//					ufox2[m]=700;
						ufoz2[m]=2;
						kills++;
						p0++;
						missilez[n]=0;
						t=randy(3)+1;
/*						if(t==1)
							FSOUND_PlaySound(FSOUND_FREE,explosion1);
						if(t==2)
							FSOUND_PlaySound(FSOUND_FREE,explosion2);
						if(t==3)
							FSOUND_PlaySound(FSOUND_FREE,explosion3);*/
					}
				missilex[n]+=2.2f;
				if(int(missilex[n])>=640)
					missilez[n]=0;
//				ritaclip(missile,surf,int(missilex[n]),int(missiley[n]),21,7);
			}
			if(int(missilez2[n])==1){
				battle=1;
				for(m=0;m<UFONR;m++)
					if((int(ufoz[m])==1)&&missilex2[n]+21>ufox[m]&&missilex2[n]<ufox[m]+41&&missiley2[n]+7>ufoy[m]&&missiley2[n]<ufoy[m]+18){
	//					ufox[m]=-60;
						ufoz[m]=2;
						kills2++;
						p1++;
						missilez2[n]=0;
						t=randy(3)+1;
/*						if(t==1)
							FSOUND_PlaySound(FSOUND_FREE,explosion1);
						if(t==2)
							FSOUND_PlaySound(FSOUND_FREE,explosion2);
						if(t==3)
							FSOUND_PlaySound(FSOUND_FREE,explosion3);*/
					}
				missilex2[n]-=2.2f;
				if(int(missilex2[n])<-21)
					missilez2[n]=0;
//				ritaclip(missile2,surf,int(missilex2[n]),int(missiley2[n]),21,7);
			}
			if(missilez[n]>1){
				battle=1;
				missilez[n]+=0.4f;
				if(int(missilez[n])-2>=30)
					missilez[n]=0;
			}
			if(missilez2[n]>1){
				battle=1;
				missilez2[n]+=0.4f;
				if(int(missilez2[n])-2>=30)
					missilez2[n]=0;
			}
		}
		for(n=0;n<UFONR;n++){ //boll dodar skepp
			if(int(ufoz[n])==1)
				if(int(ufoz[n]==1)&&boll.xpos+boll.w2>ufox[n]&&boll.xpos-boll.w2<ufox[n]+41&&boll.ypos+boll.h2>ufoy[n]&&boll.ypos-boll.h2<ufoy[n]+18){
					ufoz[n]=2;
					kills2++;
					p1++;
					t=randy(3)+1;
/*					if(t==1)
						FSOUND_PlaySound(FSOUND_FREE,explosion1);
					if(t==2)
						FSOUND_PlaySound(FSOUND_FREE,explosion2);
					if(t==3)
						FSOUND_PlaySound(FSOUND_FREE,explosion3);*/
					}
			if(int(ufoz2[n])==1)
				if(int(ufoz2[n]==1)&&boll.xpos+boll.w2>ufox2[n]&&boll.xpos-boll.w2<ufox2[n]+41&&boll.ypos+boll.h2>ufoy2[n]&&boll.ypos-boll.h2<ufoy2[n]+18){
					ufoz2[n]=2;
					kills++;
					p0++;
					t=randy(3)+1;
/*					if(t==1)
						FSOUND_PlaySound(FSOUND_FREE,explosion1);
					if(t==2)
						FSOUND_PlaySound(FSOUND_FREE,explosion2);
					if(t==3)
						FSOUND_PlaySound(FSOUND_FREE,explosion3);*/
					}
		}
		for(n=0;n<100;n++){ //boll dodar missiler
			if(int(missilez[n])==1)
				if(int(missilez[n]==1)&&boll.xpos+boll.w2>missilex[n]&&boll.xpos-boll.w2<missilex[n]+21&&boll.ypos+boll.h2>missiley[n]&&boll.ypos-boll.h2<missiley[n]+7){
					missilez[n]=2;
					t=randy(3)+1;
/*					if(t==1)
						FSOUND_PlaySound(FSOUND_FREE,explosion1);
					if(t==2)
						FSOUND_PlaySound(FSOUND_FREE,explosion2);
					if(t==3)
						FSOUND_PlaySound(FSOUND_FREE,explosion3);*/
					}
			if(int(missilez2[n])==1)
				if(int(missilez2[n]==1)&&boll.xpos+boll.w2>missilex2[n]&&boll.xpos-boll.w2<missilex2[n]+21&&boll.ypos+boll.h2>missiley2[n]&&boll.ypos-boll.h2<missiley2[n]+7){
					missilez2[n]=2;
					t=randy(3)+1;
/*					if(t==1)
						FSOUND_PlaySound(FSOUND_FREE,explosion1);
					if(t==2)
						FSOUND_PlaySound(FSOUND_FREE,explosion2);
					if(t==3)
						FSOUND_PlaySound(FSOUND_FREE,explosion3);*/
					}
		}
/*		if(FSOUND_Stream_GetTime(streeam))
			battle=1;*/
		if(!battle){
			forcebattle=0;
// 			FMUSIC_SetMasterVolume(mod,volym);
//			printf("battle end");
		}
		if(kills==UFONR){
			p0+=10;
			kills=0;
			if(SDL_GetTicks()<bonus2delay)
				bonus1delay=bonus2delay+5000;
			else
				bonus1delay=SDL_GetTicks()+5000;
		}
		if(kills2==UFONR){
			p1+=10;
			kills2=0;
			if(SDL_GetTicks()<bonus1delay)
				bonus2delay=bonus1delay+5000;
			else
				bonus2delay=SDL_GetTicks()+5000;
		}
	}
}

void drawbattle(SDL_Surface *surf)
{
	if(battle){
		for(n=0;n<UFONR;n++){
			if(int(ufoz[n])==1)
				ritaclip(ufo,surf,int(ufox[n]),int(ufoy[n]),41,18);
//				ritaclip(skepp[int(10+10*sin(ufox[n]/6.0+ripplephase/7.0))],surf,int(ufox[n]),int(ufoy[n]),40,40);
			if(int(ufoz2[n])==1)
				ritaclip(ufo2,surf,int(ufox2[n]),int(ufoy2[n]),41,18);
			if(int(ufoz[n])>1)
				ritaclip(explode[int(ufoz[n])-2],surf,int(ufox[n]),int(ufoy[n]),40,30);
			if(int(ufoz2[n])>1)
				ritaclip(explode[int(ufoz2[n])-2],surf,int(ufox2[n]),int(ufoy2[n]),40,30);
		}
		for(n=0;n<100;n++){
			if(int(missilez[n])==1)
				ritaclip(missile,surf,int(missilex[n]),int(missiley[n]),21,7);
			if(int(missilez2[n])==1)
				ritaclip(missile2,surf,int(missilex2[n]),int(missiley2[n]),21,7);
			if(int(missilez[n])>1)
				ritaclip(explode[int(missilez[n])-2],surf,int(missilex[n]),int(missiley[n]),40,30);
			if(int(missilez2[n])>1)
				ritaclip(explode[int(missilez2[n])-2],surf,int(missilex2[n]),int(missiley2[n]),40,30);
		}
	}
	if(SDL_GetTicks()<bonus1delay)
		if(SDL_GetTicks()<bonus2delay){
			if(bonus1delay<bonus2delay)
				ritaclip(bonus1,surf,640/2-427/2,480/2-62/2-100,427,62);
		}else
			ritaclip(bonus1,surf,640/2-427/2,480/2-62/2-100,427,62);
	if(SDL_GetTicks()<bonus2delay)
		if(SDL_GetTicks()<bonus1delay){
			if(bonus2delay<bonus1delay)
				ritaclip(bonus2,surf,640/2-427/2,480/2-62/2-100,427,62);
		}else
			ritaclip(bonus2,surf,640/2-427/2,480/2-62/2-100,427,62);
//	ritaclip(explode[10],surf,40,targety2,40,30);
//	ritaclip(explode[20],surf,600-41,targety,40,30);
//	ritaclip(ufo2,surf,40,targety2,41,18);
//	ritaclip(ufo,surf,600-41,targety,41,18);
}
