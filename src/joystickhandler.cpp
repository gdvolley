//
// C++ Implementation: joystickhandler
//
// Description:
//
//
// Author: Öyvind Johannessen <gathers@gmail.com>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "joystickhandler.h"

void JoystickHandler::ConfigurePlayer(spelarobjekt *players, spelarobjekt &player)
{
	SDL_Event event[3];
	player.joystick_enabled = true;
	player.ai = 0;
	int n = 0;
	while (n < 3) {
		SDL_WaitEvent(&event[n]);
		if (TestIsSet(event[n], players))
			continue;
		switch (event[n].type) {
		case SDL_JOYAXISMOTION:
// 			printf("which=%d\taxis=%d\tvalue=%d\n", event[n].jaxis.which, event[n].jaxis.axis, event[n].jaxis.value);
			if (!(event[n].jaxis.value > 32767 / 2 || event[n].jaxis.value < -32767 / 2))
				break;
			player.joystickSetAxis(n , event[n].jaxis);
			n++;
			break;
		case SDL_JOYBUTTONDOWN:
			player.joystickSetButton(n , event[n].jbutton);
			n++;
			break;
		case SDL_JOYHATMOTION:
			player.joystickSetHat(n , event[n].jhat);
			n++;
			break;
		default:
			break;
		}
	}
}


bool JoystickHandler::TestIsSet(SDL_Event &event, spelarobjekt *players)
{
	bool retval = false;

	for (int p = 0;p < 2;p++) {
		for (int n = 0;n < 3;n++)
			players[p].key[n] = false;

		switch (event.type) {
		case SDL_JOYAXISMOTION:
			players[p].feedJAxis(event.jaxis);
			break;
		case SDL_JOYBUTTONDOWN:
			players[p].feedJButton(event.jbutton);
			break;
		case SDL_JOYHATMOTION:
			players[p].feedJHat(event.jhat);
			break;
		default:
			break;
		}

		for (int m = 0;m < 3;m++) {
			if (players[p].key[m] == true)
				retval = true;
		}
	}
	return retval;
}


JoystickHandler::JoystickHandler()
{
	if (SDL_InitSubSystem(SDL_INIT_JOYSTICK)) {
		fprintf(stderr, "Error initializing joystick subsystem!\n");
		exit(-1);
	}
	joystick = new SDL_Joystick*[SDL_NumJoysticks()];
	for (int n = 0;n < SDL_NumJoysticks();n++) {
		// Open joystick
		joystick[n] = SDL_JoystickOpen(n);

		if (joystick[n]) {
			printf("Detected Joystick %d\n", n);
			printf("Name: %s\n", SDL_JoystickName(n));
			printf("Number of Axes: %d\n", SDL_JoystickNumAxes(joystick[n]));
			printf("Number of Buttons: %d\n", SDL_JoystickNumButtons(joystick[n]));
			printf("Number of Balls: %d\n", SDL_JoystickNumBalls(joystick[n]));
//			SDL_JoystickClose(joystick[n]);
		} else
			printf("Couldn't open Joystick 0\n");
	}
}


JoystickHandler::~JoystickHandler()
{
	for (int n = 0;n < SDL_NumJoysticks();n++)
		if (SDL_JoystickOpened(n)) {
			printf("closing joystick nr %d\n", n);
			SDL_JoystickClose(joystick[n]);
		}
	delete[] joystick;
}


void JoystickHandler::Configure(spelarobjekt *players, int num)
{
	SDL_JoystickEventState(SDL_ENABLE);
	ConfigurePlayer(players, players[num]);
}


