//
// Author: Öyvind Johannessen <gathers@gmail.com>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include <cstdlib>
#include <time.h>
#include "randy.h"

using namespace std;

double randy() {
	return double(rand())/(RAND_MAX+1.0);
}

int randy(int max) {
	return int(max*randy());
}

void seed_randy() {
	srand((unsigned)time(NULL));
}
