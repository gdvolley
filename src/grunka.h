//
// C++ Interface: grunka
//
// Description:
//
//
// Author: Öyvind Johannessen <gathers@gmail.com>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef grunka_h
#define grunka_h

/**
	@author Öyvind Johannessen <gathers@gmail.com>
*/
class grunka
{
public:
        float xpos;
        float ypos;
        float xspd;
        float yspd;
        int w,h,w2,h2;

        grunka();

        ~grunka();

        float distance_squared(grunka &other);
};
#endif
