//
// C++ Implementation: grunka
//
// Description:
//
//
// Author: Öyvind Johannessen <gathers@gmail.com>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "grunka.h"

grunka::grunka()
{
}


grunka::~grunka()
{
}


float grunka::distance_squared(grunka &other)
{
	float retval = (xpos - other.xpos) * (xpos - other.xpos) + (ypos - other.ypos) * (ypos - other.ypos);
	return retval;
}
