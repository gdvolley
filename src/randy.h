//
// Author: Öyvind Johannessen <gathers@gmail.com>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef randy_h
#define randy_h

double randy();
int randy(int max);
void seed_randy();

#endif
